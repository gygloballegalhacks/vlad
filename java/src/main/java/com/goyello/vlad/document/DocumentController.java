package com.goyello.vlad.document;

import fr.opensagres.xdocreport.core.XDocReportException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class DocumentController {

    private DocumentService documentService;

    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @GetMapping("document/generate")
    public String generate() throws IOException, XDocReportException {

        return documentService.generate();
    }
}
