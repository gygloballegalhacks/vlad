package com.goyello.vlad.document;

import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


@Service
public class DocumentService {
    @Value(value = "classpath:static/templates/specific_task_contract.docx")
    private Resource templateResource;

    public String generate() throws IOException, XDocReportException {
        InputStream in = templateResource.getInputStream();
        IXDocReport document = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);

        IContext context = document.createContext();
        context.put("question1", "value1");
        HashMap<String, List<String>> subject = new HashMap<>();
        subject.put("source", Arrays.asList("one", "two", "three"));
        context.put("subject", subject);

        File documentFile = new File("specific_task_contract.docx");
        document.process(context, new FileOutputStream(documentFile));
        return documentFile.getAbsolutePath();
    }
}
