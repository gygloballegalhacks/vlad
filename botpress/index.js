const question = {
  replies: ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen'],
  text: "How are you?"
}

module.exports = function(bp) {
  bp.middlewares.load()
  // GET_STARTED is the first message you get on Facebook Messenger

  bp.hear(/GET_STARTED|hello|hi|test|hey|holla/i, (event, next) => {
    event.reply('#welcome', {question.text: 'supercontent super long answer hi hello', question.replies: responseReplies.slice(0, Math.min(11, responseReplies.length))})
  })
}
