from sklearn.externals import joblib
from sklearn import svm


def train_clf(features, targets, clf_model_file_path):
    """
    Trains SVM model.
    Parameters
    ----------
    features - [list <array>] Vectors representing sentences.
    targets - [list <str/int>] Classes of sentences.
    clf_model_file_path - [str] Path to model.

    Returns
    -------
    None
    """
    clf = svm.SVC(
        kernel='linear', verbose=False
    ).fit(
        features, targets)

    joblib.dump(clf, clf_model_file_path)


def test(features, clf_model_file_path):
    """
    Tests classifier model.
    Parameters
    ----------
    features - [list <array>] Vectors representing sentences.
    clf_model_file_path - [str] Path to classifier model.

    Returns
    -------
    - [list <int/str>] Predicted classes of sentences.
    """
    clf = joblib.load(clf_model_file_path)
    return clf.predict(features)


def print_results(sentences, predicted, targets):
    """
    Prints results of classification.
    Parameters
    ----------
    sentences - [list <str>] Classified sentences.
    predicted - [list <str/int> Classes predicted by classifier.
    targets - [list <str/int> Real classes of sentecnes.

    Returns
    -------
    None
    """
    for sentence, class_, result in zip(sentences, targets, predicted):
        class_ = class_.strip() if type(class_) == str else class_
        color = u'\u001b[32m' if result == class_ else u'\u001b[31m'
        msg = u"{} {}, {}\u001b[0m, {}".format(
            color, class_, result, sentence[:50])
        print(msg)
