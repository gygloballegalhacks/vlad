import fasttext


def prepare_dataframe(dataframe, label_prefix='__label__'):
    """
    Prepares dataframe for fasttext training.
    Parameters
    ----------
    dataframe - [DataFrame] Dataframe.
    label_prefix - [str] Prefix of class (label) name.

    Returns
    -------
    - [DataFrame] Dataframe prepared for fasttext training.
    """
    # Add label prefix before each class.
    dataframe['class'] = label_prefix + dataframe['class'].astype(str) + ' '

    # Add required spacing between class and text.
    dataframe['text'] = ' ' + dataframe['text'] + ' '
    return dataframe


def train(train_set_file_path, model_file_path):
    """
    Trains fasttext model.
    Parameters
    ----------
    train_set_file_path - [str] Path to train set.
    model_file_path - [str] Path to fasttext model.

    Returns
    -------
    None
    """
    fasttext.supervised(
        input_file=train_set_file_path,
        output=model_file_path,
        label_prefix="__label__",
        epoch=100,
        ws=2,
        dim=50,
    )


def test(model_file_path, test_set_file_path):
    """
    Tests fasttext model.
    Parameters
    ----------
    model_file_path - [str] Path to fasttext model.
    test_set_file_path - [str] Path to test set.

    Returns
    -------
    None
    """
    model = fasttext.load_model(model_file_path, encoding='utf-8')
    result = model.test(test_set_file_path)
    print('Accuracy:', result.precision)
    print('Number of examples:', result.nexamples)


def predict_proba(model_file_path, sentences, classes):
    """
    Text fasttext model (with probability)
    Parameters
    ----------
    model_file_path - [str] Path to fasttext model.
    sentences - [list <str>] Texts.
    classes - [list <str>] Expected classes of texts.

    Returns
    -------
    None
    """
    model = fasttext.load_model(model_file_path, encoding='utf-8')
    results = model.predict_proba(sentences)
    for sentence, class_, result in zip(sentences, classes, results):
        class_ = class_.strip()
        predicted_class = result[0][0]
        probability = result[0][1]
        color = u'\u001b[32m' if predicted_class == class_ else u'\u001b[31m'
        msg = u"{} {}, {}\u001b[0m ({}), {}".format(
            color, class_, predicted_class, probability, sentence[:50])
        print(msg)
