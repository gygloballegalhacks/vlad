import numpy as np

from gensim.models import Word2Vec


def train_w2v(tokenized_sentences, model_file_path):
    """
    Trains word2vec model.
    Parameters
    ----------
    tokenized_sentences - [list <list>] Texts.
    model_file_path - [str] Path to word2vec model.

    Returns
    -------
    None
    """
    model = Word2Vec(
        sentences=tokenized_sentences,  # tokenized senteces, list of list of strings
        size=50,  # size of embedding vectors
        workers=4,  # how many threads?
        min_count=1,  # minimum frequency per token, filtering rare words
        sample=0.05,  # weight of downsampling common words
        sg=0,  # should we use skip-gram? if 0, then cbow
        iter=100
    )

    model.save(model_file_path)


def get_sentence_vector(word2vec_model, tokenized_sentence):
    """
    Converts word tokens into vector.
    Parameters
    ----------
    word2vec_model - [str] Path to word2vec model.
    tokenized_sentence - [list <str>] Word tokens.

    Returns
    -------
    - [array] Vector representing word tokens.
    """
    return np.mean(word2vec_model[tokenized_sentence], axis=0)


def remove_out_of_vocabulary_words(w2v_model, tokenized_sentences):
    """
    Removes word tokens that are not present in word2vec model.
    Parameters
    ----------
    w2v_model - [str] Path to word2vec model.
    tokenized_sentences - [list <str>] Word tokens.

    Returns
    -------
    - [list <str>] Word tokens known to word2vec model.
    """
    return [word for word in tokenized_sentences
            if word in w2v_model.wv.vocab]


def prepare_features(tokenized_sentences, w2v_model_file_path):
    """
    Prepares word tokens to be converted into vector with word2vec model.
    Parameters
    ----------
    tokenized_sentences - [list <list>] Word tokens.
    w2v_model_file_path - [str] Path to word2vec model.

    Returns
    -------
    - [list <array>] Vectors representing tokenized sentences.
    """

    w2v_model = Word2Vec.load(w2v_model_file_path)
    features = []
    for tokenized_sentence in tokenized_sentences:
        tokenized_sentence = remove_out_of_vocabulary_words(
            w2v_model, tokenized_sentence)
        if tokenized_sentence:
            sentence_vector = get_sentence_vector(
                w2v_model, tokenized_sentence)
            features.append(sentence_vector)
        else:
            pass
            # print "Skipping empty feature"
    return features

