# coding: utf-8

import re
import nltk
import unicodedata

STOPWORDS = ("w", "z", "od", "do", "i", "a", "o", "takze", "tak", "nie",
             "na", "przez", "we", "ich", "jest", "byc", "ma", "jako", "dla",
             "przy", "pln", "beda", "tym", "niz", "po", "oraz", "ze", "aby",
             "byly", "sa", "wg", "em", "sie", "lub", "tez", "ani", "za",
             "nad", "moze", "ten", "jego", "ta", "gdy", "jak", "badz",
             "tak", "nie", "tych", "f", "ona", "prze", "to", "co", "nr")


def mreplace(s, chararray, newchararray):
    """
    Replaces characters in a string.

    Parameters
    ----------
    s - [str] String with characters to replace.
    chararray - [list <str>] Old characters.
    newchararray - [list <str>] New characters.

    Returns
    -------
    [str] String with replaced characters.
    """
    for a, b in zip(chararray, newchararray):
        s = s.replace(a, b)
    return s


def utf2ascii(s):
    """
    Replaces UTF characters with their ASCII equivalents.
    Parameters
    ----------
    s - [str] String with characters to replace.

    Returns
    -------
    - [str] String with replaced characters.
    """
    chararray = [
        u'ą', u'ć', u'ę', u'ł', u'ń', u'ó', u'ś', u'ź', u'ż', u'č', u'ö',
        u'„', u'”', u'í'
    ]
    newchararray = [
        u'a', u'c', u'e', u'l', u'n', u'o', u's', u'z', u'z', u'c', u'o',
        u'"', u'"', u'i'
    ]
    return mreplace(s, chararray, newchararray)


def create_tokenizer():
    """
    Creates tokenizer for Polish texts.
    Returns
    -------
    - [PunktSentenceTokenizer] tokenizer
    """
    tokenizer = nltk.data.load('tokenizers/punkt/{0}.pickle'.format('polish'))
    tokenizer._params.abbrev_types.add('t.j')  # to jest
    tokenizer._params.abbrev_types.add('tj')  # to jest
    tokenizer._params.abbrev_types.add('dz')  # dziennik
    tokenizer._params.abbrev_types.add('u')  # ustaw
    tokenizer._params.abbrev_types.add('ust')  # ustep
    tokenizer._params.abbrev_types.add('ww')  # wyzej wymienione
    tokenizer._params.abbrev_types.add('mies')  # miesiac
    tokenizer._params.abbrev_types.add('sp.k')  # ???
    tokenizer._params.abbrev_types.add('kom')  # komorka
    tokenizer._params.abbrev_types.add('tel')  # telefon
    return tokenizer


def normalize_text(text):
    """
    Normalizes text.
    Parameters
    ----------
    text - [str] Text to normalize.

    Returns
    -------
    - [str] Normalized text.
    """
    text = text.lower()
    text = utf2ascii(text)
    text = unicodedata.normalize('NFKD', text).encode('ASCII', 'ignore')
    return text


def prepare_to_sent_tokenization(text):
    """
    Prepares text to be tokenized into sentences.
    Parameters
    ----------
    text - [str] Text.

    Returns
    -------
    - [str] Text prepared to sentence tokenization.
    """
    text = re.sub(r'(?m)^\s*$', '', text)  # Remove multiple white chars.
    text = re.sub(r'\d+\.', '', text)  # Decimal ordinal numbers.
    text = re.sub(r'[ivxlcm]{2,}\.', '', text)  # Roman ordinal numbers.
    return text


def clean_text(text):
    """
    Removes useless characters from text.
    Parameters
    ----------
    text - [str] Text.

    Returns
    -------
    - [str] Text without useless characters.
    """
    text = re.sub(r'\d+\.', '', text)  # Ordinal numbers.
    text = re.sub(r'\d+/\d+', '', text)  # Page numbers.

    # 1997r. -> 1997
    # viii.r. -> viii
    text = re.sub(r'r\.', '', text)

    # fax: -> fax
    # e-mail -> email
    # nazwisko** -> nazwisko
    # imie___ -> imie
    text = re.sub(r'[_\[\]()*:?\-.,]', '', text)
    text = re.sub(r'\.\.+', '', text)  # Multiple dots.
    text = re.sub(r'/', ' ', text)  # Alternatives e.g. a/b/c -> a b c.
    text = re.sub(r'\s+', ' ', text)  # Multiple white characters
    return text


def remove_stopwords(words):
    """
    Removes stopwords from list of strings.
    Parameters
    ----------
    words - [list <str>] Word tokens.

    Returns
    -------
    - [list <str>] Word tokens without stopwords.
    """
    words = [word for word in words if word.isalpha()]
    words = [word for word in words if word not in STOPWORDS]
    return words


def sentence_to_words(sentence):
    """
    Tokenizes text into words.
    Parameters
    ----------
    sentence - [str] Text.

    Returns
    -------
    - [list <str>] Word tokens.
    """
    cleaned_sentence = min_clean_text(sentence)
    words = nltk.word_tokenize(cleaned_sentence, language="polish")
    words = remove_stopwords(words)
    words = ' '.join(words)
    return words


def min_clean_text(text):
    """
    Separates punctuation character from words.
    Parameters
    ----------
    text - [str] Text.

    Returns
    -------
    - [str] Text with punctuation character separated from words.
    """
    text = text.replace('"', '')
    text = text.replace('\'', ' \' ')
    text = text.replace('.', ' . ')
    text = text.replace('(', ' ( ')
    text = text.replace(')', ' ) ')
    text = text.replace('!', ' ! ')
    text = text.replace('?', ' ? ')
    text = text.replace(':', ' ')
    text = text.replace(';', ' ')
    text = text.replace(',', '')
    return text
