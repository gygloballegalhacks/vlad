import os

import pandas as pd
from fasttext import fasttext
from settings import BASE_DIR
from tools.feature_extraction.fast_text import prepare_dataframe

TRAIN_SET_PATH = os.path.join(
    BASE_DIR, 'data', 'subject_of_contract', 'train_set.csv')
TEST_SET_PATH = os.path.join(
    BASE_DIR, 'data', 'subject_of_contract', 'test_set.csv')
FT_TRAIN_SET_PATH = os.path.join(
    BASE_DIR, 'data', 'subject_of_contract', 'fasttext', 'train_set.csv')
FT_TEST_SET_PATH = os.path.join(
    BASE_DIR, 'data', 'subject_of_contract', 'fasttext', 'test_set.csv')
CBOW_MODEL_PATH = os.path.join(
    BASE_DIR, 'models', 'subject_of_contract', 'fasttext_cbow',
    'cbow_model_dim_50.vec')
SUPERVISED_MODEL_PATH = os.path.join(
    BASE_DIR, 'models', 'subject_of_contract', 'fasttext_cbow',
    'supervised_model.bin')

# Load data set
train_df = pd.read_csv(TRAIN_SET_PATH, names=['class', 'text'])
test_df = pd.read_csv(TEST_SET_PATH, names=['class', 'text'])

train_df = prepare_dataframe(train_df)
test_df = prepare_dataframe(test_df)

train_df.to_csv(
    FT_TRAIN_SET_PATH, header=None, index=False, columns=['class', 'text'])
test_df.to_csv(
    FT_TEST_SET_PATH, header=None, index=False, columns=['class', 'text'])

# Create fasttext model
print "Word vectors"
vectors = fasttext.load_model(CBOW_MODEL_PATH, encoding='utf-8')

print "Traning classifier"
classifier = fasttext.supervised(
    input_file=FT_TRAIN_SET_PATH,
    output=SUPERVISED_MODEL_PATH,
    label_prefix="__label__",
    epoch=300,
    dim=50,
    minn=1,
    maxn=20,
    bucket=20000,
    pretrained_vectors=CBOW_MODEL_PATH
)

result = classifier.test(FT_TEST_SET_PATH)
print('Accuracy:', result.precision)
print ('Number of examples:', result.nexamples)

texts = [text.strip() for text in test_df['text']]

classes = [cls.strip() for cls in test_df['class']]
results = classifier.predict_proba(texts, 2)

for text, cls, result in zip(texts, classes, results):
    cls = cls.replace('__label__', '')
    predicted_cls = result[0][0]
    probability = result[0][1]
    color = u'\u001b[32m' if predicted_cls == cls else u'\u001b[31m'
    msg = u"{} {}, {}\u001b[0m ({}), {}".format(
        color, cls, result, probability, text)
    print(msg)


