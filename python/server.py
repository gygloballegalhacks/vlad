from flask import Flask
import intention

app = Flask(__name__)
app.register_blueprint(intention.blueprint)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
