import os
from fasttext import fasttext

from flask import jsonify, Blueprint

from settings import BASE_DIR

blueprint = Blueprint("intention", __name__)

SUPERVISED_MODEL_PATH = os.path.join(
    BASE_DIR, 'models', 'subject_of_contract', 'fasttext', 'supervised_model.bin')


@blueprint.route("/intention/<string:text>", methods=['GET'])
def get_intention(text):
    classifier = fasttext.load_model(SUPERVISED_MODEL_PATH)
    results = classifier.predict_proba([text], 2)
    clss = [result[0].replace('__label__', ' ').strip() for result in results[0]]
    probs = [result[1] for result in results[0]]
    if probs[0] > 0.95:
        intention = [clss[0]]
    else:
        intention = clss
    return jsonify({'intention': intention})
