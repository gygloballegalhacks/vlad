import fasttext
import os

from settings import BASE_DIR

TRAIN_SET_PATH = os.path.join(
    BASE_DIR, 'data', 'subject_of_contract', 'fasttext', 'train_set.csv')
SUPERVISED_MODEL_PATH = os.path.join(
    BASE_DIR, 'models', 'subject_of_contract', 'fasttext', 'supervised_model')


classifier = fasttext.supervised(
    TRAIN_SET_PATH,
    SUPERVISED_MODEL_PATH,
    dim=50,
    epoch=100,
)
