#!/usr/bin/env bash

BIN=/home/pwierzgala/Programs/fastText/fasttext
TRAIN_SET=/home/pwierzgala/vlad/python/data/subject_of_contract/fasttext/train_set.csv
CLS_MODEL=/home/pwierzgala/vlad/python/models/subject_of_contract/fasttext_cbow/cls_model
CBOW_VEC_MODEL=/home/pwierzgala/vlad/python/models/subject_of_contract/fasttext_cbow/cbow_vec_model

# Create vectors
${BIN} cbow -input ${TRAIN_SET} -output ${CBOW_VEC_MODEL}

# Train classifier
${BIN} supervised -input ${TRAIN_SET} -output ${CLS_MODEL} -dim 50 -pretrainedVectors ${CBOW_VEC_MODEL}

# Test classifier
TEST_SET=/home/pwierzgala/vlad/python/data/subject_of_contract/fasttext/test_set.csv
${BIN} predict-prob ${CLS_MODEL}.bin ${TEST_SET} 2