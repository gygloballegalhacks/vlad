import fasttext
import os

from settings import BASE_DIR

TRAIN_SET_PATH = os.path.join(
    BASE_DIR, 'data', 'subject_of_contract', 'fasttext', 'train_set.csv')
MODEL_PATH = os.path.join(
    BASE_DIR, 'models', 'subject_of_contract', 'fasttext_cbow', 'fasttext_model')


fasttext.cbow(
    TRAIN_SET_PATH,
    MODEL_PATH,
    dim=50
)
